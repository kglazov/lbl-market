<?php

namespace Tests\Feature;


use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ProductTest extends TestCase
{
    use WithoutMiddleware;


    public function testsProductsAreCreatedCorrectly()
    {
        $payload = [
            'title' => 'Yogurt',
            'title_fr' => 'Yogurt French',
            'brand' => 'Activia',
            'condition' => 'new',
            'status' => 'new',
            'quantity_available' => 400,
            'quantity_sold' => 40,
            'sku' => '098098098098',
        ];

        $response = $this->json('POST', '/api/product', $payload);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertDatabaseHas('products', $payload);
    }

    public function testsProductsAreUpdatedCorrectly()
    {
        $this->withoutMiddleware();
        $product = factory(Product::class)->create();

        $payload = [
            'title' => 'Lorem',
            'title_fr' => $product->title_fr,
            'brand' => 'Ipsum',
            'condition' => $product->condition,
            'status' => $product->status,
            'quantity_available' => $product->quantity_available,
            'quantity_sold' => $product->quantity_available,
            'sku' => $product->sku,
        ];

        $response = $this->json('PUT', '/api/product/' . $product->id, $payload);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('products', $payload);
    }

    public function testsProductsAreDeletedCorrectly()
    {
        $this->withoutMiddleware();
        $product = factory(Product::class)->create();

        $response = $this->delete('/api/product/' . $product->id);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testProductsAreListedCorrectly()
    {
        factory(Product::class)->create([
            'title' => 'First Title',
            'brand' => 'First Brand',
        ]);

        factory(Product::class)->create([
            'title' => 'Second Title',
            'brand' => 'Second Brand'
        ]);

        $response = $this->json('GET', '/api/product', []);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
