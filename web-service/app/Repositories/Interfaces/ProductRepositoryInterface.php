<?php

namespace App\Repositories\Interfaces;

use App\User;

interface ProductRepositoryInterface
{
    public function all();

    public function getByUser(User $user);
}
