<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'title_fr'      => $this->title_fr,
            'brand'         => $this->brand,
            'condition'     => $this->condition,
            'status'        => $this->status,
            'quantity_available'  => (int) $this->quantity_available,
            'quantity_sold'       => (int) $this->quantity_sold,
            'sku'           => $this->sku,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ];
    }
}
