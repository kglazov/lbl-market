<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'title' => $faker->name,
        'title_fr' => $faker->name,
        'brand' => $faker->company,
        'condition' => 'new',
        'status' => 'new',
        'quantity_available' => $faker->numberBetween(1, 500),
        'quantity_sold' => $faker->numberBetween(1,50),
        'sku' => $faker->randomNumber()
    ];
});
